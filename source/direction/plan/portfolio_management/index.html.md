---
layout: markdown_page
title: "Category Direction - Portfolio Management"
description: GitLab supports enterprise Agile portfolio and project management frameworks, including Scaled Agile Framework (SAFe), Scrum, and Kanban. Learn more!
canonical_path: "/direction/plan/portfolio_management/"
---

- TOC
{:toc}

## Portfolio Management

|                       |                               |
| -                     | -                             |
| Stage                 | [Plan](/direction/plan/)      |
| Maturity              | [Viable](/direction/maturity/) |
| Content Last Reviewed | `2023-02-03`

### Introduction and how you can help

Thanks for visiting this category direction page on Portforlio Management in GitLab. This page belongs to the [Product Planning](/handbook/product/categories/#product-planning-group) group of the Plan stage and is maintained by <Amanda Rueda>([E-Mail](mailto:<arueda@gitlab.com>) [Twitter](https://twitter.com/amandamrueda)).

This direction page is a work in progress, and everyone can contribute:

 - Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Category%3APortfolio%20Management&first_page_size=100) and [epics]((https://gitlab.com/groups/gitlab-org/-/epics?state=opened&page=1&sort=start_date_desc&label_name[]=Category:Portfolio+Management) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and want to discuss how GitLab can improve Portfolio Management tools, we'd especially love to hear from you.


### Strategy and Themes
GitLab's vision is to provide Portfolio Management tools for DevOps that help our customers manage a portfolio of work and determine which opportunities have higher ROI when making strategic business planning decisions. 

Enterprises work on complex initiatives that cut across multiple teams and departments, often spanning months, quarters, and even years. We support organizing initiatives into  multi-level work breakdown plans. We enable organizations to track efforts in flight and plan upcoming work to best utilize their resources and focus on the right priorities. 

GitLab supports popular [enterprise Agile portfolio and project management frameworks](https://about.gitlab.com/solutions/agile-delivery/), including Scaled Agile Framework [(SAFe)](https://about.gitlab.com/solutions/agile-delivery/scaled-agile/), [Scrum, and Kanban](https://about.gitlab.com/solutions/agile-delivery/).


|          |          |
| ---      | ---      |
| ![epicstree-direction.png](/direction/plan/portfolio_management/epicstree-direction.png)  | ![roadmaps-direction.png](/direction/plan/portfolio_management/roadmaps-direction.png)   |

### 1 year plan
<!--
1 year plan for what we will be working on linked to up-to-date epics. This section will be most similar to a "road-map". Items in this section should be linked to issues or epics that are up to date. Indicate relative priority of initiatives in this section so that the audience understands the sequence in which you intend to work on them. 
 -->

#### What is next for us

1. [Converting epics to work items](https://gitlab.com/groups/gitlab-org/-/epics/6033) - Epics and Issues have different data elements and behaviors, which leads to confusion for our users. Epics are only available at the group level, which significantly decreases their reach. We will collaborate with the Project Management group to build the capabilities that Epics need into the Work Items framework. Our first iteration of this was building the concept of parent/child relationships which was used for [tasks](https://docs.gitlab.com/ee/user/tasks.html#tasks). 
1. [Saved Queries and Views](https://gitlab.com/groups/gitlab-org/-/epics/5516) - It’s hard to query, save and share planning data with others. We plan to allow users to save a filtered list of their work item data so that they can easily come back to and share a list of work items. An example use cases is how at GitLab we frequently monitor our backlog for security issues and take that into account with every milestone plan. In the future, product managers and engineering team leads could have a shared saved query that surfaces all the security issues that have not been prioritized for their group.
1. Improvements to increase the maturity of [Roadmaps](https://gitlab.com/groups/gitlab-org/-/epics/2649) - Roadmaps are an industry standard way in which plans are visualized, and are key to an experience that is lovable by product and project managers. Key functionality like drag and drop editing are missing from our current implementation. 

#### What we are currently working on

1. [Filter epic lists by specific group](https://gitlab.com/gitlab-org/gitlab/-/issues/385191). It is cumbersome to find epics in lists with complex group hierarchies. Now with the ability to filter the epic list by a specific group, users can return targetted results making searching much more efficient.
1. [Auto-expanded summary](https://gitlab.com/gitlab-org/gitlab/-/issues/386937). Building off the success of [rendering a title](https://gitlab.com/gitlab-org/gitlab/-/issues/15694) when adding `+` to the end of a GitLab issue url, we are now introducing a summary version of this to display `title_assignee_milestone_health status`. This time savor is valuable when trying to convey a lot of information for little effort.
1. [Linked Epics API - Additional fields](https://gitlab.com/gitlab-org/gitlab/-/issues/386167). We're adding `created_at` and `updated_at` to the Linked Epics API. This will allow users to perform advanced queries and discover newly created linked epics via the api.
1. [Linkable titles within board sidebar](https://gitlab.com/gitlab-org/gitlab/-/issues/384089). You will now be able to click epic and issue titles from within the board sidebar improving efficiency with intuitive navigation. 
1. [Support advanced reporting with Linked Epics API](https://gitlab.com/groups/gitlab-org/-/epics/9142+). This work supports advanced querying and filtering of the Linked Epic API so that changes are easily traceable.
1. [Epic Boards v4 REST API Support](https://gitlab.com/groups/gitlab-org/-/epics/9499). We have support for Epic Boards in GraphQL but not in REST. Some customers need REST for integrations with third parties.
1. [Reorder records in work items hierarchy widgets](https://gitlab.com/groups/gitlab-org/-/epics/9548). As users add records to work item widgets like `tasks`, they may have a need to order the records in a specific way. With this work, we are creating a "drag and drop" feature to give users more flexibility.


#### What we recently completed

1. [Allow epics to have child epics and issues from different groups](https://gitlab.com/groups/gitlab-org/-/epics/8294) -  When working on large scale initiatives, contributions are needed from multiple teams which may be divided in different group hierarchies in GitLab. Today, issues and epics must be within the same group hierarchy to be added as children of an epic. We run into this limitation at GitLab when tracking work across our gitlab-com and gitlab-org groups. We will enable users to add epics from other hierarchies to facilitate tracking work across GitLab groups. **15.9**
1. [Maturing Health Status](https://gitlab.com/groups/gitlab-org/-/epics/2952) - An initial version of the Health Status was released and has received customer interest. Health Status offers a way for leaders to gain insight into risks or blockers that teams have run into. There are key enhancements that need to be implemented to make it a complete workflow and a great experience. **15.8**
1. [OKR MVC for GitLab Dogfooding](https://gitlab.com/groups/gitlab-org/-/epics/8990) - Today organizations (including GitLab) use a myriad of tools to track OKRs across the organization. This can span spreadsheets, presentations, text files in repositories, and in some cases purpose-built tools. They often also have the challenge of tying the Objectives and Key Results into initiatives (sometimes features, or software changes) - this requires careful integration between tool chains. The Product Planning group in tandem with a dedicated [SEG](https://about.gitlab.com/handbook/engineering/incubation/okr/) have shipped  an MVC for internal use as a validation step towards a viable feature we can generally release in 2023. **15.8**
1. [Group minimum role should be Guest for epic relations](https://gitlab.com/groups/gitlab-org/-/epics/9232) - Since guest users can create issues and view epics, they should have the ability to relate epics to issues to ensure a complete workflow. In this milestone, we will relax permission requirements for adding parent records. **15.8**




#### What is Not Planned Right Now

In the next two years:
- We plan to build a flexible planning tool that can be configured to implement SAFe. We do not plan to implement a system that is optimized for SAFe only. 


### Best in Class Landscape
<!-- Blanket description consistent across all pages that clarifies what GitLab means when we say "best in class" -->

BIC (Best In Class) is an indicator of forecated near-term market performance based on a combination of factors, including analyst views, market news, and feedback from the sales and product teams. It is critical that we understand where GitLab appears in the BIC landscape.

#### Key Capabilities 

| Feature |Description |
| ------ |------|
| Portfolio financial management  |Visibility and insight into funding capacity rather than projects. Instead of determining how much it will cost to achieve the next two milestones, managers determine how much capacity is required to deliver a consistent flow of value. | 
|Portfolio level planning |  Identifying which programs to invest in, and how much. Portfolios are largely trying to figure out what initiative to fund, based on when the previous one is scheduled to finish.    | 
| Program level planning  |  Breaking large deliverables into chunks that make sense for each team and coordinating the teams' work. Programs need to worry about dependencies and coordination.   | 
| Enterprise agile framework (including SAFe) |SAFe support includes the processes, roles, and artifacts that enable scaling across teams, and the ability to plan and track work and assess economic benefits using at a minimum Portfolio SAFe in SAFe v. 5.0. EAP tools may support multiple enterprise agile frameworks commonly used in the industry.|  
| Forecasting |[A forecast is a calculation about the future completion of an item or items that includes both a date range and a probability.](https://www.scrum.org/resources/blog/agile-forecasting-techniques-next-decade#:~:text=A%20forecast%20is%20a%20calculation%20about%20the%20future%20completion%20of%20an%20item%20or%20items%20that%20includes%20both%20a%20date%20range%20and%20a%20probability.) Forecasts take the progress to date of all of the programs, then make forward-looking predictions.  |
|Dependency management|Dependencies are the relationships between work that determine the order in which the work items (features, stories, tasks) must be completed by Agile teams. Dependency management is the process of actively analyzing, measuring, and working to minimize the disruption caused by intra-team and / or cross-team dependencies. |
| Roadmapping |  [Roadmaps are the glue that link strategy to tactics. They provide all stakeholders with a view of the current, near-term, and longer-term deliverables that realize some portion of the Portfolio Vision and Strategic Themes.](https://www.scaledagileframework.com/roadmap/#:~:text=Roadmaps%20are%20the%20glue%20that%20link%20strategy%20to%20tactics.%20They%20provide%20all%20stakeholders%20with%20a%20view%20of%20the%20current%2C%20near%2Dterm%2C%20and%20longer%2Dterm%20deliverables%20that%20realize%20some%20portion%20of%20the%20Portfolio%20Vision%20and%20Strategic%20Themes.)  _© Scaled Agile, Inc._  | 
| End-to-end visibility to the value stream  | This capability indicates the tool’s ability to show the progress of software throughout the value stream from ideation through to production realization of the customer and business value.|
| Collaboration  |  Collaboration tools have the highest value for distributed organizations. These tools can range from virtual boards and team rooms to threaded conversations or advanced, work-item-context chat tools.  |


#### Roadmap

Once [epics are migrated to work items](https://gitlab.com/groups/gitlab-org/-/epics/9290), we will shift our focus to Enterprise Agile Planning tools as found in [this epic](https://gitlab.com/groups/gitlab-org/-/epics/7108). The first iniatives on the roadmap include improving GitLab's [roadmap experience](https://gitlab.com/groups/gitlab-org/-/epics/2649) and exploring [saved views and queries](https://gitlab.com/groups/gitlab-org/-/epics/5516).

#### Top [1/2/3] Competitive Solutions

[**Jira Align**](https://www.atlassian.com/software/jira/align). 

##### Top 3 Competitors

1. [Jira Align](https://www.atlassian.com/software/jira/align/solutions). Jira Align (formerly known as AgileCraft) was acquired by Atlassian in early 2019. It's a cloud-based product that connects securely to one or more instances of Jira (of any flavour: Cloud, Server, or Data Center) to give insight into the state of play for all of the teams in an enterprise-level organisation. ([reference](https://www.adaptavist.com/blog/introduction-to-jira-align#:~:text=Jira%20Align%20(formerly,enterprise%2Dlevel%20organisation.)))
     - Based on strong reviews from Gartner, Forrester, Peer Insights, and my own user review, Jira Align is the **top competitor** in the Enterprise Agile Planning space due to its full support for enterprise agile frameworks such as SAFe, excellent roadmapping, forecasting, dependency management, and collaboration capabilities. While Planview and Digital.ai also scored high with analysts, I found their UX heavy and dated. Jira Align scores well in portfolio management functionality and usability.
1. [Planview AgilePlace](https://www.planview.com/products-solutions/products/agileplace/). Planview's solutions enable organizations to connect the business from ideas to impact, empowering companies to accelerate the achievement of what matters most. Planview’s full spectrum of Portfolio Management and Work Management solutions create an organizational focus on the strategic outcomes that matter and empower teams to deliver their best work, no matter how they work. The comprehensive Planview platform and enterprise success model enables customers to deliver innovative, competitive products, services, and customer experiences. Headquartered in Austin, Texas, with locations around the world, Planview has more than 1,000 employees supporting 4,000 customers and 2.4 million users worldwide. For more information, visit www.planview.com. ([reference](https://www.linkedin.com/company/planview/))
1. [Digital.ai](https://digital.ai/). Based out of Boston, Massachusetts, Digital.ai is an industry-leading technology company dedicated to helping Global 5000 enterprises achieve digital transformation goals. The company's AI-powered DevOps platform unifies, secures, and generates predictive insights across the software lifecycle. Digital.ai empowers organizations to scale software development teams, continuously deliver software with greater quality and security while uncovering new market opportunities and enhancing business value through smarter software investments. ([reference](https://www.linkedin.com/company/digitaldotai/about/#:~:text=Digital.ai%20is,smarter%20software%20investments.))

|Area of focus|GitLab|[Digital.ai](https://gitlab.com/gitlab-org/plan-stage/product-planning/competitive-research/-/issues/2#gitlab-vs-digitalai-agility-user-rating)|[Planview](https://gitlab.com/gitlab-org/plan-stage/product-planning/competitive-research/-/issues/3#gitlab-vs-planview-user-rating)|[Jira Align](https://gitlab.com/gitlab-org/plan-stage/product-planning/competitive-research/-/issues/4#gitlab-vs-jira-user-rating)|
|---|:---:|:---:|:---:|:---:|
| Portfolio financial management|⬜️ | 🟨|🟩 |🟨 |
| Portfolio level planning|⬜️ |🟩 |🟨 |🟨 |
| Program level planning| ⬜️|🟩 | 🟨| 🟩|
| Enterprise agile framework (including SAFe)| 🟨 |🟩 |🟩 |🟩 |
| Forecasting|⬜️ |🟨 |🟨 |🟩 |
| Dependency management| 🟨 |🟨 |🟨 |🟩 |
| Roadmapping| 🟨 |🟩 |🟨 |🟩 |
| End to end visibility to the value stream|🟨 |🟩 |🟨 |🟩 |
| Collaboration|🟩 |🟨 |🟨 |🟩 |

- ⬜️  lacking 
- 🟨  needs improvement
- 🟩  excels


### Target Audience

- [Cameron, Compliance Manager](https://about.gitlab.com/handbook/product/personas/#cameron-compliance-manager)
- [Parker, Product Manager](https://about.gitlab.com/handbook/product/personas/#parker-product-manager)
- [Delaney, Development Team Lead](https://about.gitlab.com/handbook/product/personas/#delaney-development-team-lead)
- [Presley, Product Designer](https://about.gitlab.com/handbook/product/personas/#presley-product-designer)
- [Sasha, Software Developer](https://about.gitlab.com/handbook/product/personas/#sasha-software-developer)







